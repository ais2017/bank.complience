#include <string>
#include <vector>
#include <time.h>
#include <ctime>
#include <stdexcept>



#include "decision_confirmation.h"
#include "test.h"
#include "client.h"
#include "worker.h"

#include "deal.h"

using namespace std;
deal::deal(int ID, worker *Wor, client *Cl1, client* Cl2, vector<test*> VT, unsigned int M)
{
    if (ID<1) ID = 0;//throw std::invalid_argument("ID must be positive");
    else{
          test* T;
          for (int i=0; i< VT.size(); i++)
          {
              T = VT[i];
              TR.push_back(*(new test_result(T)));
          }
          deal_ID = ID;
          W = Wor;
          C1 = Cl1;
          C2 = Cl2;
          money = M;
          status = 1;
          date = time(NULL);
          //tm* timeinfo = localtime(&date); для преобразования в НОРМАЛЬНЫЙ формат. Данная команда взята из интернета, ее необходимо правильно интегрировать в код
          Desicion = new decision(this,0,0,"");
          Confirmation = new confirmation(this,0,1,"");
        }
}

deal::~deal()
{
    /*for (int i = 0; i < TR.size(); i++)
    {
        TR[i].~test_result();
    }*/
    Desicion->~decision();
    Confirmation->~confirmation();
}

deal::deal()
{
    status = 1;
    date = time(NULL);
    Desicion = new decision(this,0,0,"");
    Confirmation = new confirmation(this,0,1,"");
}

int deal::get_status()
{return status;}

 int deal::get_ID()
{return deal_ID;}

time_t deal::get_deal_time()
{return date;}

int deal::get_money()
{return money;}

test_result* deal::get_results(test T)
{
    for (int i = 0; i < TR.size(); i++)
    {
        if ( *(TR[i].get_test_link()) == T ) return &TR[i];
    }
    throw std::runtime_error("This test wasn`t done for this test! ");
}

decision *deal::get_decision()
{return Desicion;}

confirmation *deal::get_conf()
{return Confirmation;}

client *deal::get_client_1()
{return C1;}

client *deal::get_client_2()
{return C2;}

worker *deal::get_worker()
{return W;}

bool deal::need_confirmation()
{
    if (money > 1000000) return 1;
    else return 0;
}

int deal::calculate_result()
{
    RESULT = 1;
    int B = 1;
    for (int i = 0; i < TR.size(); i++)
    {
        if (TR[i].get_result() == true) B = 1;
        else B = 0;
        RESULT = RESULT * B;
    }
    if (Desicion->get_decision() == true) B = 1;
    else B = 0;
    RESULT = RESULT * B;
    if (Confirmation->get_decision() == true) B = 1;
    else B = 0;
    RESULT = RESULT * B;
    return RESULT;
}
