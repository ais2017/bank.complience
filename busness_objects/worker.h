#ifndef WORKER_H
#define WORKER_H

#include <string>
#include <vector>

#include "deal.h"
using namespace std;

//class deal;

class worker
{
private:
    unsigned int worker_ID;
    string Name;
    string Surname;
    string Job;
    vector<deal*> V_D;
public:
    //vector<unsigned int>W_Deals;
    worker(int ID, string name, string surname, string job);
    ~worker();
    void set_Name(string S);
    void set_Surname(string S);
    void set_Job(string S);
    void give_Deal(deal *D);
    int get_ID();
    string get_Name();
    string get_Surname();
    string get_Job();
    vector<deal*> get_Deals();
};


#endif // WORKER_H
