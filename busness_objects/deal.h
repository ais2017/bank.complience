#ifndef DEAL_WORKER_H
#define DEAL_WORKER_H
#include <vector>

using namespace std;

class worker;
class client;
class decision;
class confirmation;
class test;
class test_result;

class deal
{
   private:
    int deal_ID;
    worker *W;
    client* C1, *C2;
    decision* Desicion = NULL;
    confirmation *Confirmation = NULL;
    vector<test_result> TR;
    unsigned int money;
    time_t date;
    int status = 0;
    int RESULT = -1;
  public:
    deal();
    deal(int ID, worker* W, client* C1, client* C2, vector<test*>, unsigned int money);
    //~deal()=default;
    ~deal();
    int get_status();
    int get_ID();
    time_t get_deal_time();
    int get_money();
    test_result* get_results(test T);
    decision* get_decision();
    confirmation* get_conf();
    client* get_client_1();
    client* get_client_2();
    worker* get_worker();
    bool need_confirmation();
    int calculate_result();
};



#endif // DEAL_H
