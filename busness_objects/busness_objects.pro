QT += core
QT -= gui

CONFIG += c++11

TARGET = busness_objects
CONFIG += console \
       staticlib
CONFIG -= app_bundle

TEMPLATE = lib

SOURCES += client.cpp \
    test.cpp \
    worker.cpp \
    decision_confirmation.cpp \
    deal.cpp

HEADERS  += client.h \
    test.h \
    worker.h \
    decision_confirmation.h \
    deal.h
