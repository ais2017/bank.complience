#-------------------------------------------------
#
# Project created by QtCreator 2018-11-08T19:41:07
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_tester_conf_dec
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

HEADERS += \
    ../decision_confirmation.h
SOURCES += tst_tester_conf_dec.cpp \
    ../decision_confirmation.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
