#include <QString>
#include <QtTest>
#include "../decision_confirmation.cpp"

class Tester_conf_dec : public QObject
{
    Q_OBJECT

public:
    Tester_conf_dec();

private Q_SLOTS:
    void T_Decision_Constructor();
    void T_Decision_Setters_Getters();
    void T_Confirmation_Constructor();
    void T_Confirmation_Setters_Getters();
    void test_deny();
    void test_accept();
};

Tester_conf_dec::Tester_conf_dec(){}

void Tester_conf_dec::T_Decision_Constructor()
{
    deal* D;
    decision DES(D,0,0,"correct test string");
    QCOMPARE(DES.get_comment(),string("correct test string"));
    QCOMPARE(DES.get_Deal(),D);
    QCOMPARE(DES.get_status(),false);
    QCOMPARE(DES.get_decision(),false);
}

void Tester_conf_dec::T_Decision_Setters_Getters()
{
    deal* D;
    decision DES(D,0,0,"not correct test string");
    DES.set_comment("correct test string");
    DES.set_status(true);
    DES.set_Decision(false);
    QCOMPARE(DES.get_comment(),string("correct test string"));
    QCOMPARE(DES.get_Deal(),D);
    QCOMPARE(DES.get_decision(),false);
    QCOMPARE(DES.get_status(),true);
    DES.set_status(1000000);
    DES.set_Decision(-1000000);
    QCOMPARE(DES.get_status(),true);
    QCOMPARE(DES.get_decision(),true);
}

void Tester_conf_dec::T_Confirmation_Constructor()
{
    deal* D;
    confirmation CONF(D,0,0,"correct test string");
    QCOMPARE(CONF.get_comment(),string("correct test string"));
    QCOMPARE(CONF.get_Deal(),D);
    QCOMPARE(CONF.get_status(),false);
    QCOMPARE(CONF.get_decision(),false);
}

void Tester_conf_dec::T_Confirmation_Setters_Getters()
{
    deal* D;
    confirmation CONF(D,0,0,"not correct test string");
    CONF.set_comment("correct test string");
    CONF.set_status(true);
    CONF.set_Decision(false);
    QCOMPARE(CONF.get_comment(),string("correct test string"));
    QCOMPARE(CONF.get_Deal(),D);
    QCOMPARE(CONF.get_status(),true);
    QCOMPARE(CONF.get_decision(),false);
    CONF.set_status(1000000);
    CONF.set_Decision(-1000000);
    QCOMPARE(CONF.get_status(),true);
    QCOMPARE(CONF.get_decision(),true);
}

void Tester_conf_dec::test_deny()
{
    decision DES(NULL,0,0,"deny D");
    confirmation CONF(NULL,0,0,"deny C");
    QCOMPARE(CONF.accept(),1);
    QCOMPARE(DES.accept(),1);
}

void Tester_conf_dec::test_accept()
{
    decision DES(NULL,0,0,"accept D");
    confirmation CONF(NULL,0,0,"accept C");
    QCOMPARE(CONF.deny(),0);
    QCOMPARE(DES.deny(),0);
}



QTEST_APPLESS_MAIN(Tester_conf_dec)

#include "tst_tester_conf_dec.moc"
