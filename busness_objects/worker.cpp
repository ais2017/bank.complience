#include <string>
#include <vector>
#include <time.h>
#include <stdexcept>



#include "worker.h"
#include "deal.h"
using namespace std;


worker::worker(int ID, string name, string surname, string job):Name(name),Surname(surname),Job(job)
{
    if (ID < 1) throw std::invalid_argument("ID must be positive");
    else worker_ID = ID;
}

worker::~worker()
{
    V_D.clear();
}

void worker::set_Name(string S)
{
    Name = S;
}

void worker::set_Surname(string S)
{
    Surname = S;
}

void worker::set_Job(string S)
{
    Job = S;
}

void worker::give_Deal(deal *D)
{
    V_D.push_back(D);
}

int worker::get_ID()
{
    return worker_ID;
}

string worker::get_Name()
{
    return Name;
}

string worker::get_Surname()
{
    return Surname;
}

string worker::get_Job()
{
    return Job;
}

vector<deal *> worker::get_Deals()
{
    return V_D;
}
