#ifndef TEST_H
#define TEST_H
#include <string>
using namespace std;

class test
{
private:
    string about;
public:
    test();
    test(string About):about(About){}
    ~test();
    friend bool operator==(const test& left, const test& right);
    int set_about(string S);
    string get_about();
    bool accept();
    bool deny();
};


class test_result
{
private:
    string comment;
    test *Tst;
    bool result = false;
public:
    test_result();
    test_result(test* T);
    test_result(test* T, string comment);
    test_result(test* T, string comment, bool result):Tst(T),comment(comment),result(result){}
    test_result(test* T, bool result);
    ~test_result();
    friend bool operator==(const test_result& left, const test_result& right);
    int set_comment(string S);
    string get_comment();
    int set_test(test* T);
    test* get_test_link();
    bool get_result();
    void accept();
    void deny();
};


#endif // TEST_H
