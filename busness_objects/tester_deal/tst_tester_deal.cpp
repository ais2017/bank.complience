#include <QString>
#include <QtTest>
#include <iostream>
#include "../deal.h"
#include "../worker.h"
#include "../test.h"
#include "../decision_confirmation.h"

void out(deal D, test T1, test T2, test T3)
{
    std::cout << "T1   rez -> " <<D.get_results(T1)->get_result() << "\n" <<
            "T2   rez -> " <<D.get_results(T2)->get_result() << "\n" <<
            "T3   rez -> " <<D.get_results(T3)->get_result() << "\n" <<
            "DES  rez -> " <<D.get_decision()->get_decision() << "\n" <<
            "CONF rez -> " <<D.get_conf()->get_decision()<< endl << "========================"<<endl;
}

class Tester_deal : public QObject
{
    Q_OBJECT

public:
    Tester_deal();

private Q_SLOTS:
    void test_setters_getters();
    void test_need_conf();
    void test_result_calculation();
    void test_getResults();


};

Tester_deal::Tester_deal(){}

void Tester_deal::test_setters_getters()
{
    worker W(5,"NM","SRNM","JB");
    client *Cl1;
    client *Cl2;
    vector<test*> VT;
    test T1("test 1");
    test T2("test 2");
    test T3("test 3");
    VT.push_back(&T1);
    VT.push_back(&T2);
    VT.push_back(&T3);
    deal D(10,&W,Cl1, Cl2,VT,1000000);
    QCOMPARE(D.get_client_1(),Cl1);
    QCOMPARE(D.get_client_2(),Cl2);
    QCOMPARE(D.get_ID(),10);
    QCOMPARE(D.get_money(),1000000);
    QCOMPARE(D.get_status(),1);
    QCOMPARE(D.get_worker(),&W);
}

void Tester_deal::test_need_conf()
{
    worker W(5,"NM","SRNM","JB");
    client *Cl1;
    client *Cl2;
    vector<test*> VT;
    test T1("test 1");
    test T2("test 2");
    test T3("test 3");
    VT.push_back(&T1);
    VT.push_back(&T2);
    VT.push_back(&T3);
    deal D1(10,&W,Cl1, Cl2,VT,1000000);
    QCOMPARE(D1.need_confirmation(),false);
    deal D2(11,&W,Cl1, Cl2,VT,2000000);
    QCOMPARE(D2.need_confirmation(),true);
}

void Tester_deal::test_result_calculation()
{
    worker W(5,"NM","SRNM","JB");
    client *Cl1;
    client *Cl2;
    vector<test*> VT;
    test T1("test 1");
    test T2("test 2");
    test T3("test 3");
    VT.push_back(&T1);
    VT.push_back(&T2);
    VT.push_back(&T3);
    deal D(10,&W,Cl1, Cl2,VT,1000000);

    D.get_results(T1)->accept();
    D.get_results(T2)->accept();
    D.get_results(T3)->accept();



    decision* DES = D.get_decision();
    confirmation* CONF = D.get_conf();
    DES->set_Decision(true);
    CONF->set_Decision(true);
    QCOMPARE(D.calculate_result(),1);
    DES->set_Decision(false);
    QCOMPARE(D.calculate_result(),0);
    DES->set_Decision(true);
    CONF->set_Decision(false);
    QCOMPARE(D.calculate_result(),0);
    CONF->set_Decision(true);
    D.get_results(T3)->deny();
    QCOMPARE(D.calculate_result(),0);


}

void Tester_deal::test_getResults()
{
    worker W(5,"NM","SRNM","JB");
    client *Cl1;
    client *Cl2;
    vector<test*> VT;
    test T1("test 1");
    VT.push_back(&T1);
    deal D(10,&W,Cl1, Cl2,VT,1000000);
    test_result TR(&T1);
    QCOMPARE(*(D.get_results(T1)),TR);
}



QTEST_APPLESS_MAIN(Tester_deal)

#include "tst_tester_deal.moc"
