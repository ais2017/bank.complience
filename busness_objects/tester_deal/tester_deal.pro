#-------------------------------------------------
#
# Project created by QtCreator 2018-11-08T19:59:32
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_tester_deal
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

INCLUDEPATH += "../"
HEADERS += \
    ../deal.h \
    ../worker.h \
    ../test.h \
    ../decision_confirmation.h
SOURCES += tst_tester_deal.cpp \
    ../deal.cpp \
    ../worker.cpp \
    ../test.cpp \
    ../decision_confirmation.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
