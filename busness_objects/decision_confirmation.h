#ifndef DECISION_CONFIRMATION_H
#define DECISION_CONFIRMATION_H
class deal;

using namespace std;

class decision
{
   private:
    deal *Deal;
    //unsigned int Deal_ID;
    bool DealDecision;
    bool status;
    string comment;
public:
    //decision(unsigned int D, bool status, bool DealDes, string comment):Deal_ID(D),status(status),DealDecision(DealDes),comment(comment){}
    decision(deal *D, bool status, bool DealDes, string comment):Deal(D),status(status),DealDecision(DealDes),comment(comment){}
    int set_status(bool B)
    {
        if (B == true)
          status = true;
        else status = false;
    }
    int set_Decision(bool B)
    {
        if (B == true)
          DealDecision = true;
        else DealDecision = false;
    }
    int set_comment(string S)
      {comment = S;}
    /*unsigned int get_Deal()
    {
        return Deal_ID;
    }*/
    deal* get_Deal()
    {
        return Deal;
    }
    bool get_status()
      {return status;}
    bool get_decision()
      {return DealDecision;}
    string get_comment()
      {return comment;}
    int accept();
    int deny();
    ~decision();
};


class confirmation
{
   private:
    deal *Deal;
    //unsigned int Deal_ID;
    bool DealDecision;
    bool status;
    string comment;
public:
    //confirmation(unsigned int D, bool status, bool DealDes, string comment):Deal_ID(D),status(status),DealDecision(DealDes),comment(comment){}
    confirmation(deal *D, bool status, bool DealDes, string comment):Deal(D),status(status),DealDecision(DealDes),comment(comment){}
    ~confirmation();
    int set_status(int B)
    {
        if (B != 0)
          status = true;
        else status = false;
    }
    int set_Decision(int B)
    {
        if (B != 0)
          DealDecision = true;
        else DealDecision = false;
    }
    int set_comment(string S)
      {comment = S;}
    bool get_status()
      {return status;}
    bool get_decision()
      {return DealDecision;}
    string get_comment()
      {return comment;}
    /*unsigned int get_Deal()
    {
        return Deal_ID;
    }*/
    deal* get_Deal()
    {
        return Deal;
    }
    int accept();
    int deny();
};



#endif // DECISION_CONFIRMATION_H
