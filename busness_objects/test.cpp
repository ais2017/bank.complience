#include <string>
#include <vector>
#include <time.h>



#include "test.h"
using namespace std;
test::test()
{
    about = "";
}

test::~test()
{
    about = "";
}

bool operator==(const test &left, const test &right)
{
    //if (left.about == right.about) return true;
    //else return false;
    return left.about == right.about;
}

int test::set_about(string S)
{
    about = S;
    return 0;
}

string test::get_about()
{
    return about;
}

bool test::accept()
{
    return true;
}

bool test::deny()
{
    return false;
}







test_result::test_result()
{
    Tst = NULL;
    comment = "";
    result = false;

}

test_result::test_result(test *T):Tst(T)
{
    comment = T->get_about();
    result = false;
}

test_result::test_result(test *T, string comment):Tst(T),comment(comment)
{
    result = false;
}

test_result::test_result(test *T, bool result):Tst(T),result(result)
{
    comment = T->get_about();
}

test_result::~test_result()
{
    comment = "";
    result = false;
}

bool operator==(const test_result &left, const test_result &right)
{
    return ((left.comment == right.comment)&&
            (left.result == right.result)&&
            (left.Tst == right.Tst));
}

int test_result::set_comment(string S)
{
    comment = S;
    return 0;
}

string test_result::get_comment()
{
    return comment;
}

int test_result::set_test(test *T)
{
    if (T!=NULL)
    {
        Tst=T;
        return 0;
    }
    else return -1;
}

test *test_result::get_test_link()
{
    return Tst;
}

bool test_result::get_result()
{
    return result;
}

void test_result::accept()
{
    this->result = true;
}

void test_result::deny()
{
    //string msg = this->comment;
    this->result = false;
}
