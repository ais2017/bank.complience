#include <QString>
#include <QtTest>
#include <iostream>
#include "../worker.h"
#include "../deal.h"



class Tester_worker : public QObject
{
    Q_OBJECT

public:
    Tester_worker();

private Q_SLOTS:
    void Getters_Setters_Test();
    void vector_test();
};

Tester_worker::Tester_worker()
{
}

void Tester_worker::Getters_Setters_Test()
{
    worker W(1,"NM","SRNM","J");
    QCOMPARE(W.get_ID(),1);
    QCOMPARE(W.get_Name(),string("NM"));
    QCOMPARE(W.get_Surname(),string("SRNM"));
    QCOMPARE(W.get_Job(),string("J"));
    W.set_Job("J J");
    W.set_Name("NNMM");
    W.set_Surname("SSRRNNMM");
    QCOMPARE(W.get_Name(),string("NNMM"));
    QCOMPARE(W.get_Surname(),string("SSRRNNMM"));
    QCOMPARE(W.get_Job(),string("J J"));
    try {
        worker W(-10,"NM","SRNM","J");
    }
    catch( const std::exception& e )
    {
        QCOMPARE(e.what(),"ID must be positive");
    }
}

void Tester_worker::vector_test()
{
    deal D1, D2, D3;
    worker W(1,"NM","SRNM","J");
    vector<deal*> DealV;
    DealV.push_back(&D1);
    DealV.push_back(&D2);
    DealV.push_back(&D3);
    W.give_Deal(&D1);
    W.give_Deal(&D2);
    W.give_Deal(&D3);
    QCOMPARE(W.get_Deals(),DealV);

}

QTEST_APPLESS_MAIN(Tester_worker)

#include "tst_tester_worker.moc"
