#-------------------------------------------------
#
# Project created by QtCreator 2018-11-14T13:16:45
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_tester_worker
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

LIBS += -L../../build-busness_objects-Desktop_Qt_5_6_3_GCC_64bit-Debug -lbusness_objects


INCLUDEPATH += "../"
HEADERS += \
    ../deal.h \
    ../worker.h \
    ../test.h \
    ../decision_confirmation.h
SOURCES += tst_tester_worker.cpp\
    ../deal.cpp \
    ../worker.cpp \
    ../test.cpp \
    ../decision_confirmation.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
