#-------------------------------------------------
#
# Project created by QtCreator 2018-11-19T11:53:44
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_test_deal_v2t
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

INCLUDEPATH += "../"
HEADERS += \
    ../deal.h \
    ../worker.h \
    ../test.h \
    ../decision_confirmation.h
SOURCES += tst_test_deal_v2t.cpp\
    ../deal.cpp \
    ../worker.cpp \
    ../test.cpp \
    ../decision_confirmation.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
