#include <QString>
#include <QtTest>
#include <iostream>
#include "../deal.h"
#include "../worker.h"
#include "../test.h"
#include "../decision_confirmation.h"

void out(deal D, test T1, test T2, test T3)
{
    std::cout << "T1   rez -> " <<D.get_results(T1)->get_result() << "\n" <<
            "T2   rez -> " <<D.get_results(T2)->get_result() << "\n" <<
            "T3   rez -> " <<D.get_results(T3)->get_result() << "\n" <<
            "DES  rez -> " <<D.get_decision()->get_decision() << "\n" <<
            "CONF rez -> " <<D.get_conf()->get_decision()<< endl << "========================"<<endl;
}

class Test_deal_V2T : public QObject
{
    Q_OBJECT

public:
    Test_deal_V2T();

private Q_SLOTS:
    void test_setters_getters();
    void test_need_conf();
    void test_result_calculation();
    void test_getResults();
};

Test_deal_V2T::Test_deal_V2T()
{
}

void Test_deal_V2T::test_setters_getters()
{
    worker W(5,"NM","SRNM","JB");
    client *Cl1;
    client *Cl2;
    vector<test*> VT;
    test T1("test 1");
    test T2("test 2");
    test T3("test 3");
    VT.push_back(&T1);
    VT.push_back(&T2);
    VT.push_back(&T3);
    deal D1(10,&W,Cl1, Cl2,VT,1000000);
    //QCOMPARE(D1.need_confirmation(),false);
    //deal D2(11,&W,Cl1, Cl2,VT,2000000);
    //QCOMPARE(D2.need_confirmation(),true);
}

void Test_deal_V2T::test_need_conf()
{

}

void Test_deal_V2T::test_result_calculation()
{

}

void Test_deal_V2T::test_getResults()
{

}

QTEST_APPLESS_MAIN(Test_deal_V2T)

#include "tst_test_deal_v2t.moc"
