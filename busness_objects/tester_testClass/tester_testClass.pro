#-------------------------------------------------
#
# Project created by QtCreator 2018-11-08T20:09:39
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_tester_testclass
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_tester_testclass.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
