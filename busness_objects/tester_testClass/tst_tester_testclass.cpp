
#include <string>
#include <QtTest>
#include "../test.cpp"

class Tester_testClass : public QObject
{
    Q_OBJECT

public:
    Tester_testClass();

private Q_SLOTS:
    void T_Test_Constructor_Setters_Getters();
    void T_Test_EQUAL();
    void T_Test_accept();
    void T_Test_Deny();
    void T_TestResult_Constructor();
    void T_TestResult_Constructor_TEST();
    void T_TestResult_Constructor_TEST_STRING();
    void T_TestResult_Constructor_TEST_STRING_RESULT();
    void T_TestResult_Constructor_TEST_RESULT();
    void T_TestResult_Setters_Getters();
    void T_TestResult_Accept();
    void T_TestResult_Deny();
};

Tester_testClass::Tester_testClass()
{
}

void Tester_testClass::T_Test_Constructor_Setters_Getters()
{
    test TST_1;
    QCOMPARE(TST_1.get_about(),string(""));
    TST_1.set_about("correct test string");
    QCOMPARE(TST_1.get_about(),string("correct test string"));
    test TST_2(" << testing comstructor >> ");
    QCOMPARE(TST_2.get_about(),string(" << testing comstructor >> "));
}

void Tester_testClass::T_Test_EQUAL()
{
    test TST_1("EQUAL");
    test TST_2("EQUAL");
    QCOMPARE((TST_1 == TST_2),true);
    TST_2.set_about("NON EQUAL");
    QCOMPARE((TST_1 == TST_2),false);
}

void Tester_testClass::T_Test_accept()
{
    test T("accept");
    QCOMPARE(T.accept(),true);
}

void Tester_testClass::T_Test_Deny()
{
    test T("deny");
    QCOMPARE(T.deny(),false);
}

void Tester_testClass::T_TestResult_Constructor()
{
    test_result TR;
    QCOMPARE(TR.get_comment(),string(""));
    QCOMPARE(TR.get_result(),false);
    //QCOMPARE(TR.get_test_link(),NULL);
}

void Tester_testClass::T_TestResult_Constructor_TEST()
{
    test TST("correct test string");
    test_result TR(&TST);
    QCOMPARE(TR.get_comment(),string("correct test string"));
    QCOMPARE(TR.get_result(),false);
    QCOMPARE(TR.get_test_link(),&TST);
    QCOMPARE(*(TR.get_test_link()),TST);
}

void Tester_testClass::T_TestResult_Constructor_TEST_STRING()
{
    test TST("incorrect test string");
    test_result TR(&TST,string("correct test string"));
    QCOMPARE(TR.get_comment(),string("correct test string"));
    QCOMPARE(TR.get_result(),false);
    QCOMPARE(TR.get_test_link(),&TST);
    QCOMPARE(*(TR.get_test_link()),TST);
}

void Tester_testClass::T_TestResult_Constructor_TEST_STRING_RESULT()
{
    test TST("incorrect test string");
    test_result TR(&TST, "correct test string",true);
    QCOMPARE(TR.get_comment(),string("correct test string"));
    QCOMPARE(TR.get_result(),true);
    QCOMPARE(TR.get_test_link(),&TST);
    QCOMPARE(*(TR.get_test_link()),TST);
}

void Tester_testClass::T_TestResult_Constructor_TEST_RESULT()
{
    test TST("correct test string");
    test_result TR(&TST, false);
    QCOMPARE(TR.get_comment(),string("correct test string"));
    QCOMPARE(TR.get_result(),false);
    QCOMPARE(TR.get_test_link(),&TST);
    QCOMPARE(*(TR.get_test_link()),TST);
}

void Tester_testClass::T_TestResult_Setters_Getters()
{
    test TST("incorrect test string");
    test TST_2("another test");
    test_result TR(&TST, "correct test string",true);
    TR.set_comment("new correct test string");
    TR.set_test(&TST_2);
    QCOMPARE(TR.get_comment(),string("new correct test string"));
    QCOMPARE(TR.get_result(),true);
    QCOMPARE(TR.get_test_link(),&TST_2);
    QCOMPARE(*(TR.get_test_link()),TST_2);
}

void Tester_testClass::T_TestResult_Accept()
{
    test TST("correct test string");
    test_result TR(&TST, false);
    TR.accept();
    QCOMPARE(TR.get_result(),true);
}

void Tester_testClass::T_TestResult_Deny()
{
    test TST("correct test string");
    test_result TR(&TST, true);
    TR.deny();
    QCOMPARE(TR.get_result(),false);
}

QTEST_APPLESS_MAIN(Tester_testClass)

#include "tst_tester_testclass.moc"
